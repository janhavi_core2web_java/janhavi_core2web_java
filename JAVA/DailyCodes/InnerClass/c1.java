class Outer {

    void run() {
        class Inner {
            Inner() {
                System.out.println("In Inner Constructor");
            }
        }
        System.out.println("In run");
        Inner obj = new Inner();
    }

    void fun() {
        System.out.println("In fun");
        run();  
    }

    Outer() {
        System.out.println("In Outer Constructor");
    }

    public static void main(String[] args) {
        Outer outobj = new Outer();
        outobj.fun();
     
    }
}

