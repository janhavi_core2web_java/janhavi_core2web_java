class FloatDemo
{
	public static void main(String[] args)
	{
		float x = 5.6f;				//4bytes
		System.out.println(x);
		
		double y = 7.6;				//8bytes
		System.out.println(y);
	}
}
