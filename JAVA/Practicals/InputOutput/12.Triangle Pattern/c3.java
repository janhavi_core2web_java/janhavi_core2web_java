import java.util.Scanner;

class ReverseAlphabetPattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int numRows = scanner.nextInt();

        // Loop to print the pattern
        for (int i = 1; i <= numRows; i++) {
            char currentChar = (char)(64+numRows); // Starting character for each row

            // Loop to print characters for each row
            for (int j = 1; j <= i; j++) {
                System.out.print(currentChar);

                // Decrement the character for the next iteration
                currentChar--;

                // Add space if not the last character on the row
                if (j < i) {
                    System.out.print(" ");
                }
            }
            System.out.println(); // Move to the next line after each row is printed
        }

        scanner.close();
    }
}

