import java.util.Scanner;

class AlphabetPattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int numRows = scanner.nextInt();
	char currentChar = (char) (65+numRows);
	for (int i = 0; i < numRows; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print((char)(currentChar) + " ");
                currentChar++;
            }
            System.out.println();
        }
    }
}
