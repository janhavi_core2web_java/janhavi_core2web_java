import java.util.Scanner;

class TrianglePattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int R = scanner.nextInt();

        
        for (int i = 1; i <= R; i++)
        {
           
            for (int j = 1; j <= R; j++)
            {
                System.out.print("");
            }
           
            for (int k = 1; k <= i; k++)
            {
            	System.out.print(R*R +" ");
            }
            System.out.println();
        }

        //scanner.close();
    }
}

