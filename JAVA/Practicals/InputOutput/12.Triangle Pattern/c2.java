import java.util.Scanner;

class AlternatePattern1
{
    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int numRows = scanner.nextInt();

        
        // Loop to print the pattern
        for (int i = 1; i <= numRows; i++) 
        {
            // If it's an odd-numbered row, print 'a', else print '$'
            if (i % 2 == 1) 
            {
            	char currentChar = 'a'; // Starting character
                for (int j = 1; j <= i; j++) 
                {
                    

                    System.out.print(currentChar);
                    // Add space if not the last character on the row
                    if (j < i) 
                    {
                        System.out.print(" ");
                    }
                    // Increment current character
                    currentChar++;
                }
            } 
            else 
            {
                for (int j = 1; j <= i; j++) 
                {
                    System.out.print("$");
                    // Add space if not the last character on the row
                    if (j < i) 
                    {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println(); // Move to the next line after each row is printed
        }

        //scanner.close();
    }
}

