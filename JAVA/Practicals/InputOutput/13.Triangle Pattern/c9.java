import java.util.*;

class demo9
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		
		int n = (R*R)+(R-1);
		
		for(int i = 1; i<=R ;i++)
		{
			
			for (int j=1;j<=R-i+1;j++)
			{
			
				System.out.print(n+"\t");
				n-=2;
				
			}
			System.out.println();
		}
	}
}


