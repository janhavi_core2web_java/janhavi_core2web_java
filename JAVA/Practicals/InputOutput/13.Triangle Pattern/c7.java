import java.util.*;

class demo7
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		int ch= 65+R;
		int n = R;
		
		for(int i = 1; i<=R ;i++)
		{
			char newch = (char)(ch-i);
			int newn = n;
			for (int j=1;j<=R-i+1;j++)
			{
				if(j%2==1)
				{
					System.out.print(newn+"\t");
					newn--;
					newch--;
				}
				else
				{
					System.out.print(newch +"\t");
					newch--;
					newn--;
				}
				
			}
			//ch--;
			n--;
			System.out.println();
		}
	}
}

/*4	c	2	a
3	b	1
2	a
1*/
