import java.util.Scanner;

class demo1
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int R = sc.nextInt();

        for (int i = 1; i <= R; i++)
        { 
            for (int j = i; j <= R; j++)
            { 
                System.out.print(j + " "); 
            }
            System.out.println(); 
        }
    }
}

