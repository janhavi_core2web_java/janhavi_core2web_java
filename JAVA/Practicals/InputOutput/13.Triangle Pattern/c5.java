import java.util.*;

class demo5
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		char ch = 65;
		
		for(int i =1; i<=R;i++)
		{
			char newch= ch;
			for (int j=i;j<=R;j++)
			{
				if(i%2==1)
				{
					System.out.print(newch+"\t");
				}
				else
				{
					System.out.print((char)(newch+32) +"\t");
				}
				newch++;
			}
			System.out.println();
		}
	}
}
