import java.util.*;

class demo6
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		
		
		for(int i = 1; i<=R ;i++)
		{
			char ch= 65;
			int n = 1;
			for (int j=1;j<=R-i+1;j++)
			{
				if(j%2==1)
				{
					System.out.print(n+"\t");
					n++;
				}
				else
				{
					System.out.print(ch +"\t");
					ch++;
				}
				
			}
			System.out.println();
		}
	}
}
