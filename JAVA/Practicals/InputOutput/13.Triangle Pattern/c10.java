import java.util.*;

class demo10
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		int ch= 65+R;
		
		for(int i = 1; i<=R ;i++)
		{
			char newch = (char)(ch-i);
			for (int j=1;j<=R-i+1;j++)
			{
				if(i%2==0)
				{
					System.out.print((char)(newch+32)+"\t");
					newch--;
				}
				else
				{
					System.out.print(newch +"\t");
					newch--;
				
				}
				
			}
			
			System.out.println();
		}
	}
}


