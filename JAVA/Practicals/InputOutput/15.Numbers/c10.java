import java.util.*;

class demo10
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		int n = num;
		int reversed = 0;
        	while (n!= 0) 
        	{
            		int digit = n % 10;
            		reversed = reversed * 10 + digit;
            		n/= 10;
		}
		
		if(num==reversed)
		{
			System.out.println(num+" is a palindrome Number");
		}
		
		else
		{
			System.out.println(num+" is not a palindrome Number");
		}
			
	}
}
