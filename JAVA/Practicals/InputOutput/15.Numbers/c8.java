import java.util.*;

class demo8
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		int n = num;
		int reversed = 0;
        	while (n!= 0) 
        	{
            		int digit = n % 10;
            		reversed = reversed * 10 + digit;
            		n/= 10;
		}
		
		System.out.println("Reversed Number of "+num+" : "+reversed);	
	}
}
