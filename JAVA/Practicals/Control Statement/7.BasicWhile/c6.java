class alphabet
{
	public static void main(String args[])
	{
		char current = 'A';
		//string vowel = {A,E,I,O,U};
		
		while(current<= 'Z')
		{
			if(isVowel(current)){
			current++;
			continue;
			}
			else{
			System.out.print(current +" ");
			}
			current++;
		}
		System.out.println();
	}
	public static boolean isVowel(char ch) 
	{
        	ch = Character.toUpperCase(ch);
        	return ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U';
    	}
}
