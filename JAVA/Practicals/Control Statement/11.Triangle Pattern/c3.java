import java.util.*;

class demo3
{
	public static void main(String args[])
	{
		 Scanner sc = new Scanner(System.in);
		 System.out.print("Enter no. of rows : ");
		 int R = sc.nextInt();
		 char ch = 'A';
		 for (int i = 1; i <= R; i++) 
		 { 
          		char newch = ch;
            		for (int j = 1; j <= i; j++) 
            		{ 
                		System.out.print(newch + " ");
                		newch++; 
            		}
            		
            		System.out.println(); 
            		ch++;
        	}
	}
}
