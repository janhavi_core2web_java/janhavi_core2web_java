import java.util.*;

class demo9
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		 
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		
		char ch = (char)(64+R);
		
		for (int i = R; i >= 1; i--) 
		{ 
            		char newch = ch;
            		for (int j = 1; j <= i; j++) 
            		{ 
                		System.out.print(newch + " ");
                		newch--; 
            		}
            		System.out.println();
            		
        	}
	}
}
