import java.util.*;

class demo8
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter number of rows: ");
        int R = sc.nextInt();
        
        for (int i = 1; i <= R; i++)
        { 
            int n = i;
            for (int j = i; j <= R; j++)
            { 
                System.out.print(n + " "); 
                n++;
            }
            System.out.println(); 
        }
    }
}
