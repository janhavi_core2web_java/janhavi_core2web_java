import java.util.*;

class demo7
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		 
		System.out.print("Enter no. of rows : ");
		int R = sc.nextInt();
		 
		for (int i = R; i >= 1; i--) 
		{ 
            		for (int j = 1; j <= i; j++) 
            		{ 
                		System.out.print(j + " "); 
            		}
            		System.out.println();
        	}
	}
}
