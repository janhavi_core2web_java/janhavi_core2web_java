import java.util.*;

class demo10
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter number of rows: ");
        int R = sc.nextInt();
        int ch = 65;
        
        if(R%2==1)
        {
        for (int i = 1; i <= R; i++)
        { 
            int newch =ch;
            for (int j = i; j <= R; j++)
            { 
                if(j%2==0)
                {
                	System.out.print(newch + "\t"); 
                }
                else
                {
                	System.out.print((char)(newch) + "\t"); 
                }
                newch++;
            }
            ch++;
            System.out.println(); 
        }
        }
       	
       	else
       	{
       	for (int i = 1; i <= R; i++)
        { 
            int newch =ch;
            for (int j = i; j <= R; j++)
            { 
                if(j%2==1)
                {
                	System.out.print(newch + "\t"); 
                }
                else
                {
                	System.out.print((char)(newch) + "\t"); 
                }
                newch++;
            }
            ch++;
            System.out.println(); 
        }	
       	}
    }
}
