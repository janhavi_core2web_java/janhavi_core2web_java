import java.util.Scanner;

class GradeCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int totalMarks = 0;
        int passingMarks = 40; // Assuming passing marks for each subject
        int passedSubjects = 0;

        for (int i = 1; i <= 5; i++) {
            System.out.print("Enter marks for subject " + i + ": ");
            int marks = scanner.nextInt();

            if (marks >= passingMarks) {
                passedSubjects++;
                totalMarks += marks;
            } else {
                System.out.println("You failed in subject " + i + ". Marks obtained: " + marks);
            }
        }

        if (passedSubjects != 5) {
            for (int i = 1; i <= 5; i++) {
                if (i <= passedSubjects) {
                    continue;
                }
                System.out.println("You failed in subject " + i);
                System.out.println("Grade: F");
                System.out.println("You are failed in exam.");
            }
        } else {
            System.out.println("Total Marks: " + totalMarks);

            int averageMarks = totalMarks / 5;
            switch (averageMarks / 10) {
                case 10:
                    System.out.println("Grade: O");
                    break;
                case 9:
                    System.out.println("Grade: A+");
                    break;
                case 8:
                    System.out.println("Grade: A");
                    break;
                case 7:
                    System.out.println("Grade: B");
                    break;
                case 6:
                    System.out.println("Grade: C");
                    break;
                case 5:
                    System.out.println("Grade: D");
                    break;
                /*default:
                    System.out.println("Grade: F");
                    System.out.println("You are failed in exam.");
                    break;*/
            }
        }
    }
}

