import java.util.Scanner;

class TwoNumbers
{
    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Number 1: ");
        int num1 = scanner.nextInt();
        
        System.out.print("Enter Number 2: ");
        int num2 = scanner.nextInt();
        
        if(num1>=00 && num2>=0)
        {
        	System.out.println("Multiplication : "+ num1*num2);
        	
        	if(num1%2==0 && num2%2==1)
        	{
        		System.out.println(num1 +" is even");
        		System.out.println(num2 +" is odd");
        	}
        	
        	else if(num1%2==1 && num2%2==0)
        	{
        		System.out.println(num1 +" is odd");
        		System.out.println(num2 +" is even");
        	}
        	
        	else if(num1%2==0 && num2%2==0)
        	{
        		System.out.println(num1+ " & " + num2 +" both are even");
        		
        	}
        	
        	else 
        	{
        		System.out.println(num1 +" & " + num2 +" both are odd");
        	}
        }
        else
        {
        	System.out.println("Sorry negative number is not allowed");
        }
    }
}
