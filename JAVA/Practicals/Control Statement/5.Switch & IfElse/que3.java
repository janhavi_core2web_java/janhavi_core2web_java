import java.util.Scanner;

class SizeCheck {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your size: ");
        String size = scanner.nextLine();

        switch (size) {
            case "XS":
                System.out.println("Extra small");
                break;
            case "S":
                System.out.println("Small");
                break;
            case "M":
                System.out.println("Medium");
                break;
            case "L":
                System.out.println("Large");
                break;
            case "XL":
                System.out.println("Extra large");
                break;
            default:
                System.out.println("Size not found");
        }
    }
}

