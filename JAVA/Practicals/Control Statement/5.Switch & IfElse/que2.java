import java.util.Scanner;
class GradeRemark 
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your grade: ");
        char grade = scanner.next().charAt(0);
        

        switch (grade) {
            case 'O':
                System.out.println("Outstanding");
                break;
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very Good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'P':
                System.out.println("Pass");
                break;
            case 'F':
                System.out.println("Fail");
                break;
    
            default:
                System.out.println("Invalid Grade.Plz enter O, A, B, C, P, F out of this");
        }
    }
}

