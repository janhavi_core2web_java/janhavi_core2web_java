class Character {
    public static void main(String[] args) {
        char ch = 'A';

        if (ch >= 'a' && ch <= 'z') {
            System.out.println(ch + " is lowercase character");
        } else {
            System.out.println(ch + " is uppercase character");
        }
    }
}

