import java.util.*;

class demo6
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter the number of rows: ");
        	int R = sc.nextInt();
        
  
        	for (int i=0;i<R;i++)
        	{
        		char ch = 'A';
        		int n = 1 ;
        		for (int j=0;j<R;j++)
        		{
        			System.out.print(ch +""+n+" ");
        			ch++;
        			n++;
        		}
        		System.out.println();
        	}

	}
}
