import java.util.*;

class demo9
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter the number of rows: ");
        	int R = sc.nextInt();
        	
  
        	for (int i=0;i<R;i++)
        	{
        		int n = 1 ;
        		n = n + i*2;
        		for (int j=0;j<R;j++)
        		{
        			
        			System.out.print(n+"\t");
        			n++;
        		}
        		System.out.println();
        	}

	}
}
