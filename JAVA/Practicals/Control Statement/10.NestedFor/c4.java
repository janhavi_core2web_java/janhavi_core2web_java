import java.util.*;

class demo4
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter the number of rows: ");
        	int R = sc.nextInt();
        
        	char ch = 'A';
        
        	for (int i=0;i<R;i++)
        	{
        		for (int j=0;j<R;j++)
        		{
        			System.out.print(ch +" ");
        			ch+=2;
        		}
        		System.out.println();
        	}

	}
}
