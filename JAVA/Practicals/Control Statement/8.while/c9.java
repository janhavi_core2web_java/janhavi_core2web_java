class whileDemo9
{
	public static void main(String[] args)
	{
		int num =214367689;
		int count1 = 0;
		int count2 = 0;
		
		while(num>0)
		{
			int digit = num %10;
			if (digit%2!=0)
			{
				count1++;	
			}
			else
			{
				count2++;
			}
			num/=10;
			
		
		}
		System.out.println("Odd Count = " + count1);
		System.out.println("Even Count = " + count2);	
	}
}
