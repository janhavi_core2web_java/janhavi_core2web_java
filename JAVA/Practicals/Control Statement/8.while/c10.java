class whileDemo10
{
	public static void main(String[] args)
	{
		long num = 9307922405l;
		int sum = 0;
		while(num>0)
		{
			sum += num % 10;
			num/=10;
		}
		System.out.println("Sum of Digits in 9307922405 is " + sum);
			
	}
}
