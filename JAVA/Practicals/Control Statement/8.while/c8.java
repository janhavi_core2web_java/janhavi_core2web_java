class whileDemo8
{
	public static void main(String[] args)
	{
		int num =216983;
		int evencount =0;
		int oddcount = 0;
	
		while(num>0)
		{
			int digit = num%10;
			
			if (digit%2==1)
			{
				System.out.println("Odd Count : " + oddcount);
			}
			else
			{
				System.out.println("even Count : " + evencount);
			}
			num/=10;
		}
			
	}
}
