import java.util.Scanner;

class demo7 
{

    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter array size: ");
        int size = sc.nextInt();

        char[] arr = new char[size];

        System.out.println("Enter array elements:");
        for (int i = 0; i < size; i++) 
        {
            arr[i] = sc.next().charAt(0);
        }

        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] >= 'a' && arr[i] <= 'z') 
            {
                arr[i] = (char) (arr[i] - 32);
            }
        }
        
        for (char c : arr)
        {
            System.out.print(c + " ");
        }
    }
}
