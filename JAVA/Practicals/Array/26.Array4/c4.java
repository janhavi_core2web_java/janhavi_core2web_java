
import java.util.*;

class demo4
{

	public static void main(String[] args) 
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
		int count=0;
		
		System.out.println("Enter an element:");
		int num = sc.nextInt();
	
		for(int i=0;i<arr.length;i++)
		{
	
			if(arr[i]==num) 
			{
				count++;
			}
		}
		System.out.println("Count : " + count);
	
		if(count==2) 
		{
			System.out.println(num + " occures 2 times");
		}
		
		else  if (count>2)
		{
			System.out.println(num+" occures more than 2 times");
		}
		
		else 
		{
			System.out.println(num+" occures less than 2 times");
		}
	}
}
