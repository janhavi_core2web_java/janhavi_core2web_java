import java.util.*;

class demo6
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
	
	
		System.out.println("Enter array Size:");
		int size= sc.nextInt();
	
		char arr[] = new char[size];
	
		System.out.println("Enter array Element:");
		for(int i = 0; i<size; i++)
		{
			arr[i] =sc.next().charAt(0);
			//arr[i] = Character.toLowerCase(arr[i]);
			//System.out.println(arr[i]);
		}
	
		int vowcnt=0;
		int concnt=0;
	
		for(int j = 0; j<size/2; j++)
		{
			if(arr[j]=='a'|| arr[j]=='e'|| arr[j]=='i'|| arr[j]=='o'|| arr[j]=='u' )
			{
				vowcnt++;
			}
		
			else
			{
				concnt++;
			}
		}
		System.out.println("Count of vowel :" + vowcnt);
		System.out.println("Count of consonant :" + concnt);
	}
}
