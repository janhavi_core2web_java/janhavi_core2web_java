import java.util.*;

class demo3
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
		int max1 =arr[0];
		for(int i=0; i<arr.length;i++)
		{
			if (arr[i]>max1) 
			{
				max1 = arr[i];
			}
		}
		
		int max2=arr[0];
	 	for(int i=0; i<arr.length;i++)
	 	{
	 		if(arr[i]>max2 && arr[i]<max1)
	 		{
	 			max2=arr[i];
	 		}
	 
	 	}
	 	
	 	System.out.println("Second Largest Element in the Array is : " +max2);
	 }
}
