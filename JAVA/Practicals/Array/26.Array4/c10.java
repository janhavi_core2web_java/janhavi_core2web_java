import java.util.*;

class demo10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
       
        System.out.println("Enter Array size:");
        int size = sc.nextInt();
        sc.nextLine();
        
        char[] arr = new char[size];
        
        System.out.println("Enter Array elements (characters):");
        for (int i = 0; i < size; i++) {
            System.out.print("Element [" + (i + 1) + "]: ");
            arr[i] = sc.nextLine().charAt(0);
        }
        
        System.out.print("Key : ");
        char key = sc.nextLine().charAt(0);
        
        System.out.println("\nOutput : ");
        System.out.print("Array : ");
        for (int i = 0; i < size; i++) {
            if(arr[i]==key){
            	break;
            }
            else
            {
            	System.out.print(arr[i]+" ");
            }
        }
    }
        
}
