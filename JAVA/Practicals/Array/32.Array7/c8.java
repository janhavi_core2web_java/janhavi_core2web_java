import java.util.*;

class demo8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter the number of rows:");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns:");
        int cols = sc.nextInt();

   
        int[][] array = new int[rows][cols];

   
        System.out.println("Enter the elements of the 2D array:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print("Element [" + (i+1) + "][" + (j+1) + "]: ");
                array[i][j] = sc.nextInt();
            }
        }

       
        int sum = 0;
        for (int i = 0; i < rows; i++) {
            sum += array[i][cols - 1 - i];
        }

    
        System.out.println("Sum of Secondary diagonal elements: " + sum);
    }
}

