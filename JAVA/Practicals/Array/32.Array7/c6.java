import java.util.*;

class demo6{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

      
        System.out.println("Enter the number of rows:");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns:");
        int cols = sc.nextInt();

     
        int[][] array = new int[rows][cols];

       
        System.out.println("Enter the elements of the 2D array:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print("Element [" + (i+1) + "][" + (j+1) + "]: ");
                array[i][j] = sc.nextInt();
            }
        }

        System.out.print("The elements which is divisible by 3 are : ");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                 if(array[i][j]%3==0)
                 {
                 	 System.out.print(array[i][j]+"\t");
                 }
                 else{
                 	continue;
                 }
            }
           
        }
    }
}

