import java.util.*;

class demo3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

       
        System.out.println("Enter the number of rows:");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns:");
        int cols = sc.nextInt();

  
        int[][] array = new int[rows][cols];

        
        System.out.println("Enter the elements of the 2D array:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print("Element [" + i + "][" + j + "]: ");
                array[i][j] = sc.nextInt();
            }
        }

     
        System.out.println("The Sum of 2D array is:");
        for (int i = 0; i < rows; i++) {
            int sum = 0; 
            for (int j = 0; j < cols; j++) {
                sum += array[i][j];
            }
            System.out.println("Row " + i + ": " + sum);
        }
    }
}

