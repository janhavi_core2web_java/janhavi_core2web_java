import java.util.*;

class demo10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

       
        System.out.println("Enter the number of rows:");
        int rows = sc.nextInt();
        System.out.println("Enter the number of columns:");
        int cols = sc.nextInt();

        
        int[][] array = new int[rows][cols];

    
        System.out.println("Enter the elements of the 2D array:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print("Element [" + (i+1) + "][" + (j+1) + "]: ");
                array[i][j] = sc.nextInt();
            }
        }

       
        System.out.println("Corner elements of the array:");
        System.out.println("Top-left: " + array[0][0]);
        System.out.println("Top-right: " + array[0][cols - 1]);
        System.out.println("Bottom-left: " + array[rows - 1][0]);
        System.out.println("Bottom-right: " + array[rows - 1][cols - 1]);
    }
}

