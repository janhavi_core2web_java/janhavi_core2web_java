//question 2

import java.util.*;

class demo8
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the age of employee "+i+": ");
			arr[i]= sc.nextInt();
		}
		System.out.println("Ages of Employees of a Company : ");
		for(int i=0;i<arr.length;i++)
		{
			System.out.println("Employee "+i+" :" +arr[i]);
		}
	
	}
}
