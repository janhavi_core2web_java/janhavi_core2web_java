import java.util.*;

class demo10
{
	public static void main(String[] args)
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
	
		outerloop:
		for(int i=0; i<arr.length; i++)
		{
			int fact=1;
			
			for (int j = 1; j <=arr[i]; j++) 
			{
            			fact*=j;
			}
			System.out.println("Factorial of "+arr[i]+" is "+fact);	
			
			

		}
	}
} 
