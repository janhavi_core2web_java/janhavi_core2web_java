import java.util.*;

class demo5
{
	public static void main(String[] args) 
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
		for(int i=0; i<arr.length; i++)
		{
			int digit=0;
			int num=arr[i];
			
			while (num>0)
			{
				num/=10;
				digit++;
			}
		
			System.out.println(digit);
		}
	}
	
}
