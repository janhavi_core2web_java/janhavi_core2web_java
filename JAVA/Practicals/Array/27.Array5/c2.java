import java.util.*;

class demo2
{
	public static void main(String[] args) 
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
	
		int evensum=0; 
		int oddsum=0;
	
		for(int i =0;i<arr.length; i++)
		{
			if(arr[i]%2==0)
			{
				evensum +=arr[i];
			}
			
			else 
			{
				oddsum +=arr[i];
			}
		}
	
		System.out.println("Even sum : " + evensum);
		
		System.out.println("Odd sum : " + oddsum);
	}
}
