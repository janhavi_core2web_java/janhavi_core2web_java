import java.util.*;

class demo6
{
	public static void main(String[] args)
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
	
	
		outerloop:
		for(int i=0; i<arr.length; i++)
		{
			int cnt=0;
			
			for (int j = 1; j <=arr[i]; j++) 
			{
            			if (arr[i] % j == 0) 
            			{
                			cnt++;
            			}
            			else{
            				continue;
            			}
			}
				
			if(cnt <= 2)
			{
				System.out.println("First Prime Number found at index " +i);
				break outerloop;
			}
			else{
				System.out.println("No Prime Number in Array ");
				break outerloop;
			}
			

		}
	}
} 
