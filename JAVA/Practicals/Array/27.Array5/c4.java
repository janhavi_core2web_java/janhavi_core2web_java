import java.util.*;

class demo4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);    
        System.out.println("Enter Array size:");
        int size = sc.nextInt();
    
        int arr[] = new int[size];
    
        System.out.println("Enter Array elements:");
    
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
        
        int duplicateIndex = -1;

        outerLoop:
        for (int i = 0; i < arr.length; i++) {
            int dup = arr[i];
    
            for (int j = i + 1; j < arr.length; j++) {
                if (dup == arr[j]) {
                    duplicateIndex = i;
                    break outerLoop;
                }
            }
        }
        
        if (duplicateIndex != -1) {
            System.out.println("The First Duplicate element is present at index " + duplicateIndex);
        } else {
            System.out.println("No duplicate elements found.");
        }
    }
}

