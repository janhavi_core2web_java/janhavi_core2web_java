import java.util.*;

class demo9 
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter a number:");
        int number = sc.nextInt();
        
        String numberStr = Integer.toString(number);
        
        int[] digits = new int[numberStr.length()];
        
        for (int i = 0; i < numberStr.length(); i++)
        {
            digits[i] = Character.getNumericValue(numberStr.charAt(i)) + 1;
        }
        
   
        System.out.println("Number as an incremented array: " + Arrays.toString(digits));
    }
}

