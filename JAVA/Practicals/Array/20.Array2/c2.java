import java.util.*;

class demo2
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the Number "+i+": ");
			arr[i]= sc.nextInt();
		}
		System.out.println("Divisible by 3 : ");
		int sum = 0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]%3==0)
			{
				System.out.println(arr[i]+" ");
				sum+=arr[i];
			}

			else{
				continue;
			}
		}
		System.out.println("Sum of elements divisible by 3 is : "+ sum);
	}
}
