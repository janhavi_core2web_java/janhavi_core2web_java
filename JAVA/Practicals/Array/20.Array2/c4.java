import java.util.*;

class demo4
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the element "+i+": ");
			arr[i]= sc.nextInt();
		}
	
		System.out.println("Enter the number to search in array : ");
		int x = sc.nextInt();
		
		for(int i=0;i<arr.length;i++)
		{	
			if(arr[i]==x)
			{	
				System.out.println(arr[i]+" is found at index "+i);
				
			}

			else{
				continue;
			}
			
		}
		
	
	}
}
