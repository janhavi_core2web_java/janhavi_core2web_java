import java.util.*;

class demo1
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the Number "+i+": ");
			arr[i]= sc.nextInt();
		}
		System.out.println("Even Numbers : ");
		int count = 0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]%2==0)
			{
				System.out.println(arr[i]);
				count++;
			}

			else{
				continue;
			}
		}
		System.out.println("Count of Even Numbers : "+ count);
	}
}
