import java.util.*;

class demo9
{

	public static void main(String[] args)
	{
	
	Scanner sc = new Scanner(System.in);
	
	System.out.println("Enter Array Size :" );
	
	int size = sc.nextInt();
	int arr[] =new int[size];
	
	for(int i=0; i<size;i++){
		arr[i]=sc.nextInt();
	}
	
	int min=arr[0];
	
	for(int j=0; j<size; j++){
		
		if(arr[j]< min){
		min = arr[j];
	}
	}
	
	System.out.println("Minimum element  is : " +min);
	}
}
