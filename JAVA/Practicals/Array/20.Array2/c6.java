import java.util.*;

class demo6
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the element "+i+": ");
			arr[i]= sc.nextInt();
		}
	
		
		int sum = 0;
		for(int i=0;i<arr.length;i++)
		{	
			if(i%2==1)
			{	
				sum+=arr[i];
				
			}

			else{
				continue;
			}
			
		}
		System.out.println("Sum of odd indexed  Elements : "+ sum);
	
	}
}
