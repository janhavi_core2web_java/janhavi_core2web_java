import java.util.*;

class demo8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
       
        System.out.println("Enter Array size:");
        int size = sc.nextInt();
        sc.nextLine();
        
        char[] arr = new char[size];
        
        System.out.println("Enter Array elements (characters):");
        for (int i = 0; i < size; i++) {
            System.out.print("Element [" + (i + 1) + "]: ");
            arr[i] = sc.nextLine().charAt(0);
        }
   
        char[] arr2 = new char[size];
        for (int i = 0; i < size; i++) {
            arr2[i] = arr[size - 1 - i];
        }
        
        System.out.println("Reversed Array : " + Arrays.toString(arr2));
        
        System.out.println("\nOutput : ");
        int halfSize = (size + 1) / 2; 
       
        char[] arr3 = new char[halfSize];
        int index = 0;
        for (int i = 0; i < size; i += 2) {
            arr3[index++] = arr[i];
        }
        
        System.out.println("Before Reverse Array : " + Arrays.toString(arr3));
        
        char[] arr4 = new char[halfSize];
        index = 0;
        for (int i = 0; i < size; i += 2) {
            arr4[index++] = arr2[i];
        }
        
        System.out.println("After Reversed Array : " + Arrays.toString(arr4));
    }
}

