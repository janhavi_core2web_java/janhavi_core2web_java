import java.util.Scanner;

class demo9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size :");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        System.out.println("Enter Array element:");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println("\nOutput : ");
        int cnt = 0;
        for (int i = 0; i < size; i++) {
            int temp = arr[i];
            int rev = 0;
            int rem;
            
            while (temp != 0) {
                rem = temp % 10;
                rev = rem + rev * 10;
                temp = temp / 10; // corrected this line
            }
            
            
            if (arr[i] == rev) {
                cnt++;
                System.out.println(arr[i] + " is a Palindrome Number");
            }
        }
        
        System.out.println("No. of Palindrome Number: " + cnt);
    }
}

