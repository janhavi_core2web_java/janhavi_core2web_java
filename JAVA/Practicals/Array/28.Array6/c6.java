import java.util.*;

class demo6
{
	
	public static void main(String[] args) 
	{
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size :");
		int size = sc.nextInt();
		int arr[] = new int[size];
		
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
		
		System.out.println("Enter key:");
		int val = sc.nextInt();
		
		System.out.println("\nOutput :");
		for(int i=0; i<arr.length;i++)
		{
		
			if(arr[i]%val ==0) 
			{
			
				System.out.println("An element multiple of "+val+" found at index : "+i);
			}
			else
			{
				System.out.println("Element not found");
			}
		}
	}
}
