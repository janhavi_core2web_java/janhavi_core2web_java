import java.util.*;

class demo10
{
	public static void main(String[] args) 
	{
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
		
		System.out.println("\nOutput : ");
		int max1 = arr[0];
		for(int i=1; i<arr.length;i++) 
		{
			if(arr[i] >=max1)
			{
				max1 = arr[i];
			}
	
		}
		
		int max2=arr[0];
		for(int i=1; i<arr.length;i++) 
		{
			if(arr[i] >=max2 && arr[i] < max1 )
			{
				max2 = arr[i];
			}
	
		}
		
		int max3=arr[0];
		for(int i=1; i<arr.length;i++) 
		{
			if(arr[i] >=max3 && arr[i] < max2 )
			{
				max3 = arr[i];
			}
	
		}
		System.out.println("Third Largest Element is : "+max3);
		
	}
}

