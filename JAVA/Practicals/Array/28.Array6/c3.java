import java.util.*;

class demo3
{

	public static void main(String[] args)
	{
	
		Scanner sc = new Scanner(System.in);	
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
	
		int arr[] =new int[size];
	
		System.out.println("Enter Array element:");
	
		for(int i=0;i<size;i++)
		{
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the key :");
	
		int key = sc.nextInt();
	
		int cnt=0;
		
		outerloop:
		for(int i= 0; i<arr.length; i++)
		{
			if(arr[i] == key)
			{
				cnt++;
			}
			else
			{
				System.out.println(arr[i] + " element is not found ");
				break outerloop;
			}
		}
		
		if(cnt>2)
		{
		
			for(int i= 0; i<arr.length; i++)
			{
				if(arr[i]==key)
				{
		
					arr[i] = key*key*key;
					
				}
			}
		}
		System.out.println("Array will be like : ");
		for(int i= 0; i<arr.length; i++)
		{
			System.out.println(arr[i] + " ");
		}
	}
}

