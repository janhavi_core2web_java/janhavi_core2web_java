import java.util.*;

class demo2 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size: ");
        int size = sc.nextInt();

        int arr[] = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter the Number " + i + ": ");
            arr[i] = sc.nextInt();
        }

        System.out.print("Specific number: ");
        int num = sc.nextInt();
        
	int i;
        for ( i = 0; i < arr.length; i++) {
            if (num == arr[i]) {
                System.out.println(num + " is found at index " + i);
         
                break;
            }
        }

        if (i == arr.length) {
            System.out.println(num + " not found in array");
        }
    }
}

