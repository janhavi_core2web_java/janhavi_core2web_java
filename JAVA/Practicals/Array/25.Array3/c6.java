import java.util.*;

class demo6
{
	
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
        	System.out.print("Enter the size: ");
        	int size = sc.nextInt();

        	char arr[] = new char[size];
        	
        	for (int i = 0; i < arr.length; i++) 
        	{
            		System.out.print("Enter the Character" + i + ": ");
            		arr[i] = sc.next().charAt(0);
       		}
	
		for(int i=0;i<arr.length; i++)
		{
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' ) 
			{
		
				continue;
			}
			else 
			{
				System.out.println(arr[i]);
			}
		}
	}
}
