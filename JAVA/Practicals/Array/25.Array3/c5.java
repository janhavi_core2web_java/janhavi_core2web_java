import java.util.*;

class demo5
{
	public static void main(String args[])
	{
	 	Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		for (int i=0;i<arr.length;i++)
		{
			System.out.print("Enter the Number "+ i +" : ");
			arr[i]= sc.nextInt();
		}
		
		
		for (int i=0;i<arr.length;i++)
		{
			if(arr[i]<0)
			{
				System.out.print(arr[i]*arr[i]+" ");
			}
			
			else
			{
				System.out.print(arr[i]+" ");
			}
		}
	}
}
