import java.util.Scanner;

class demo8 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size: ");
        int size = sc.nextInt();

        int arr[] = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter the Number " + i + ": ");
            arr[i] = sc.nextInt();
        }

        System.out.println("Composite numbers in the array are: ");
        for (int i = 0; i < arr.length; i++) {
            int count = 0;
            for (int j = 1; j <= arr[i]; j++) {
                if (arr[i] % j == 0) {
                    count++;
                }
            }
            if (count > 2) {
                System.out.println(arr[i]);
            }
        }
    }
}

