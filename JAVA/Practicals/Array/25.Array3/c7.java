import java.util.*;

class demo7 
{
    public static void main(String args[]) 
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size: ");
        int size = sc.nextInt();

        int arr[] = new int[size];
        for (int i = 0; i < arr.length; i++) 
        {
            System.out.print("Enter the Number " + i + ": ");
            arr[i] = sc.nextInt();
        }

        
        if (arr.length % 2 == 0) 
        {
            
            for (int i = 0; i < arr.length; i++) 
            {
                
                if (arr[i] % 2 == 0)
                {
                    
                    System.out.println(arr[i]);
                }
            }
        } 
        
        else 
        {
            for (int i = 0; i < arr.length; i++) 
            {
               
                if (arr[i] % 2 == 1)
                {
                    
                    System.out.print(arr[i]+" ");
                }
            }
            
        }
    }
}

