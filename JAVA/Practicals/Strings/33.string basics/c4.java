import java.util.*;

class Demo4
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("String 1 :");
		String str1 = sc.nextLine();
		
		System.out.print("String 2 :");
		String str2 = sc.nextLine();
		
		System.out.println("Difference : " + str1.compareTo(str2));
	}
}
